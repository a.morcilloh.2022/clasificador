#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys

def es_correo_electronico(string):

    if '@' in string:
        separar = string.split('@')
        if len(separar) == 2:
            (nombre, pagina) = separar
        if nombre and pagina:
            if '.' in pagina:
                return True
    return False

def es_entero(string):

    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(entrada):

    try:
        float(entrada)
        return True
    except ValueError:
        return False

def evaluar_entrada(string):

    if not (string):
        print('Escribe una string por favor, lo que has escrito no lo es.')
        return None
    elif es_correo_electronico(string):
        return 'Es un correo electrónico.'
    elif es_entero(string):
        return 'Es un entero.'
    elif es_real(string):
        return 'Es un número real.'
    else:
        return 'No es ni un correo, ni un entero, ni un número real.'

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
